"""
Tests for the AutoDoodle script using the nose testing framework.

"""

from nose.tools import assert_equal, assert_in, assert_not_in, assert_raises, assert_regex, assert_true, raises, with_setup

import AutoDoodle
import configparser
import datetime
import io


"""
Global variables and fixtures to help run the tests.

"""
event_details = {} # Dictionary to hold details for a test event.
current_date = datetime.date(2015, 3, 17) # Tuesday
test_file_holder = [] # Holds a faked file for testing.
valid_email_list = {AutoDoodle.TO: ["participant_emails"]}
valid_email_list_all = {
    AutoDoodle.TO: ["to_participant_emails"],
    AutoDoodle.CC: ["cc_participant_emails"],
    AutoDoodle.BCC: ["bcc_participant_emails"]
}

def setup_event():
    """
    Setup a simple, non empty event.

    """
    event_details['location'] = "somewhere"


def determineRequiredFieldValue(required_field):
    """
    Helper function to determine what the value of the given required field should be.

    """
    value = "arbitrary_value"
    if required_field == AutoDoodle.ADMIN_EMAIL:
        value = "fake@email"
    elif required_field == AutoDoodle.DAY:
        value = "Tuesday"
    elif required_field == AutoDoodle.TIME:
        value = "5pm-7pm"

    return value


def setup_event_with_all_required_fields():
    """
    Setup an event with all the required fields from a configuration file filled in.

    """
    for required_field in AutoDoodle.REQUIRED_FIELDS:
        value = determineRequiredFieldValue(required_field)
        event_details[required_field] = value


def setup_empty_file():
    """
    Setup an empty faked file.

    """
    test_file_holder.append(io.StringIO())


def setup_event_config_file_with_all_required_fields():
    """
    Setup a faked event configuration file with all the required fields filled in.

    Note: If you use the StringIO object created by this fixture, call seek(0, io.SEEK_END) on it to write to the end of the stream.

    """
    faked_file = io.StringIO()
    faked_file.write("[{0}]\n".format(AutoDoodle.EVENT_DETAILS_SECTION))
    for required_field in AutoDoodle.REQUIRED_FIELDS:
        value = determineRequiredFieldValue(required_field)
        faked_file.write("{0}={1}\n".format(required_field, value))
    faked_file.seek(0, io.SEEK_SET)
    test_file_holder.append(faked_file)


def teardown_event():
    """
    Clear the event details after each test is done.

    """
    event_details.clear()


def teardown_faked_file():
    """
    Clear the faked file after each test is done.

    """
    try:
        test_file_holder[0].close()
    finally:
        del test_file_holder[:]


def verifyEmails(email_list, expected_count, expected_emails):
    """
    Helper function to check that the emails for the given header field are correct.

    @param email_list - the list of emails to verify.
    @param expected_count - the number of emails expected to be found for the given header field in the given file object.
    @param expected_emails - emails expected to be found for the given header field in the given file object.

    """
    assert_equal(expected_count, len(email_list))
    for expected_email in expected_emails:
        assert_in(expected_email, email_list)


"""
Tests for validateEventDetails().

"""
@with_setup(setup_event, teardown_event)
def test_validateEventDetails_with_required_fields():
    required_fields = ['location']
    AutoDoodle.validateEventDetails(required_fields, event_details)


@with_setup(setup_event, teardown_event)
def test_validateEventDetails_no_required_fields():
    required_fields = []
    AutoDoodle.validateEventDetails(required_fields, event_details)


@with_setup(setup_event, teardown_event)
def test_validateEventDetails_missing_required_field():
    required_fields = ['missing_required_field']
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.validateEventDetails(required_fields, event_details)
    assert_in("missing the configuration value for missing_required_field", str(error.exception))


@with_setup(setup_event, teardown_event)
def test_validateEventDetails_contains_reserved_word():
    required_fields = []
    event_details[AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE] = "fake_url"
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.validateEventDetails(required_fields, event_details)
    assert_in(AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE + " cannot be placed in the event configuration file", str(error.exception))


@with_setup(setup_event, teardown_event)
def test_validateEventDetails_blank_required_field():
    required_fields = ['blank_field']
    event_details['blank_field'] = ""
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.validateEventDetails(required_fields, event_details)
    assert_in("the value for blank_field can not be blank", str(error.exception))


@with_setup(setup_event, teardown_event)
def test_validateEventDetails_invalid_email_format():
    required_fields = [AutoDoodle.ADMIN_EMAIL]
    event_details[AutoDoodle.ADMIN_EMAIL] = "not_an_email"
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.validateEventDetails(required_fields, event_details)
    assert_in("administrator email provided does not appear to have a valid format", str(error.exception))


@with_setup(setup_event, teardown_event)
def test_validateEventDetails_invalid_day_of_the_week_for_the_event():
    required_fields = [AutoDoodle.DAY]
    event_details[AutoDoodle.DAY] = "invalid_day"
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.validateEventDetails(required_fields, event_details)
    assert_in("the day for the event must be one of:", str(error.exception))



"""
Tests for calculateDayDelta().

"""
def test_calculateDayDelta_current_day():
    assert_equal(0, AutoDoodle.calculateDayDelta(current_date, "Tuesday"))


def test_calculateDayDelta_day_in_past():
    assert_equal(-1, AutoDoodle.calculateDayDelta(current_date, "Monday"))


def test_calculateDayDelta_day_in_future():
    assert_equal(2, AutoDoodle.calculateDayDelta(current_date, "Thursday"))


def test_calculateDayDelta_day_at_week_boundary():
    current_date = datetime.date(2015, 3, 16) # Monday
    assert_equal(6, AutoDoodle.calculateDayDelta(current_date, "Sunday")) # Monday is the beginning of a week.


@raises(ValueError)
def test_calculateDayDelta_invalid_day():
    AutoDoodle.calculateDayDelta(current_date, "not_a_day")



"""
Tests for determineEventDate().

"""
def test_determineEventDate_day_of_week_for_the_event_is_today_so_schedule_for_today():
    assert_equal(current_date, AutoDoodle.determineEventDate(current_date, "Tuesday"))


def test_determineEventDate_day_of_week_for_the_event_has_already_past_so_schedule_for_next_week():
    next_monday = current_date + datetime.timedelta(days=6)
    assert_equal(next_monday, AutoDoodle.determineEventDate(current_date, "Monday"))


def test_determineEventDate_day_of_week_for_the_event_is_upcoming_so_schedule_for_this_week():
    this_thursday = current_date + datetime.timedelta(days=2)
    assert_equal(this_thursday, AutoDoodle.determineEventDate(current_date, "Thursday"))


def test_determineEventDate_day_for_the_event_goes_into_next_year():
    current_date = datetime.date(2015, 12, 30) # Last Wednesday of the year 2015.
    first_tuesday_of_next_year = current_date + datetime.timedelta(days=6)
    assert_equal(first_tuesday_of_next_year, AutoDoodle.determineEventDate(current_date, "Tuesday"))


@raises(ValueError)
def test_determineEventDate_invalid_day_of_the_week_for_the_event():
    AutoDoodle.determineEventDate(current_date, "not_a_day")



"""
Tests for getEventDate().

"""
def test_getEventDate_valid_day_of_the_week():
    event_date = AutoDoodle.getEventDate("Tuesday")
    assert_regex(event_date, "\d{8}")
    # Check to make sure that the date falls within a week of the current date.
    lower_bound = datetime.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
    upper_bound = lower_bound + datetime.timedelta(days=6)
    event_as_date_object = datetime.datetime.strptime(event_date, "%Y%m%d")
    assert_true(lower_bound <= event_as_date_object)
    assert_true(event_as_date_object <= upper_bound)


@raises(ValueError)
def test_getEventDate_invalid_day_of_the_week():
    AutoDoodle.getEventDate("not_a_day")



"""
Tests for parseEventDetails().

"""
@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_has_all_required_fields():
    required_fields = AutoDoodle.REQUIRED_FIELDS
    AutoDoodle.parseEventDetails(required_fields, test_file_holder[0])


@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_no_required_fields():
    required_fields = []
    AutoDoodle.parseEventDetails(required_fields, test_file_holder[0])


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEventDetails_empty_event_config_file():
    required_fields = []
    with assert_raises(KeyError) as error:
        AutoDoodle.parseEventDetails(required_fields, test_file_holder[0])
    assert_equal("'EventDetails'", str(error.exception))


@with_setup(setup_empty_file, teardown_faked_file)
@raises(configparser.MissingSectionHeaderError)
def test_parseEventDetails_no_section_headers_but_have_options_specified():
    required_fields = []
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.write("option=value\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEventDetails_missing_event_details_section():
    required_fields = []
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.write("[AnotherSection]\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    with assert_raises(KeyError) as error:
        AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)
    assert_in("'EventDetails'", str(error.exception))


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEventDetails_missing_day_config_value():
    required_fields = []
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.write("[EventDetails]\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    with assert_raises(KeyError) as error:
        AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)
    assert_in("'{0}'".format(AutoDoodle.DAY), str(error.exception))


@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_missing_required_field():
    required_fields = ['missing_required_field']
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEventDetails(required_fields, test_file_holder[0])
    assert_in("missing the configuration value for missing_required_field", str(error.exception))


@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_blank_required_field():
    required_fields = ['blank_field']
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.seek(0, io.SEEK_END)
    faked_event_config_file.write("blank_field=\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)
    assert_in("the value for blank_field can not be blank", str(error.exception))


@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_config_file_contains_reserved_word():
    required_fields = []
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.seek(0, io.SEEK_END)
    faked_event_config_file.write(AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE + "=value\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)
    assert_in(AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE + " cannot be placed in the event configuration file", str(error.exception))


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEventDetails_invalid_email_format_for_admin_email():
    required_fields = [AutoDoodle.ADMIN_EMAIL]
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.write("[EventDetails]\n")
    faked_event_config_file.write(AutoDoodle.ADMIN_EMAIL + "=not_an_email\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)
    assert_in("administrator email provided does not appear to have a valid format", str(error.exception))


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEventDetails_invalid_day_of_the_week_for_the_event():
    required_fields = [AutoDoodle.DAY]
    faked_event_config_file = test_file_holder[0]
    faked_event_config_file.write("[EventDetails]\n")
    faked_event_config_file.write(AutoDoodle.DAY + "=not_a_day\n")
    faked_event_config_file.seek(0, io.SEEK_SET)
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEventDetails(required_fields, faked_event_config_file)
    assert_in("the day for the event must be one of:", str(error.exception))


@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_check_calculated_event_date_added():
    required_fields = []
    section_proxy = AutoDoodle.parseEventDetails(required_fields, test_file_holder[0])
    assert_in(AutoDoodle.DATE, section_proxy)


@with_setup(setup_event_config_file_with_all_required_fields, teardown_faked_file)
def test_parseEventDetails_check_keys_for_options_are_case_sensitive():
    required_fields = []
    section_proxy = AutoDoodle.parseEventDetails(required_fields, test_file_holder[0])
    assert_in(AutoDoodle.ADMIN_NAME, section_proxy)
    assert_not_in(AutoDoodle.ADMIN_NAME.swapcase(), section_proxy)



"""
Tests for getEventDetails().

"""
@raises(IOError)
def test_getEventDetails_event_config_file_does_not_exist():
    AutoDoodle.getEventDetails(AutoDoodle.REQUIRED_FIELDS, "invalid_file")



"""
Tests for createDoodleUrl().

"""
@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createDoodleUrl_with_all_required_fields():
    event_details[AutoDoodle.DATE] = "20150317"
    assert_true(AutoDoodle.createDoodleUrl(event_details).startswith(AutoDoodle.DOODLE_CREATE_URL + "?"))


@with_setup(setup_event_with_all_required_fields, teardown_event)
@raises(KeyError)
def test_createDoodleUrl_missing_date_field():
    AutoDoodle.createDoodleUrl(event_details)



"""
Tests for getButtonXPath().

"""
def test_getButtonXPath():
    xpath = AutoDoodle.getButtonXPath("section_id", "button_class")
    assert_in("section", xpath)
    assert_in("[@type='button']", xpath)
    assert_in("[@id='section_id']", xpath)
    assert_in("[contains(@class, 'button_class')]", xpath)



"""
Tests for parseEmails().

"""
@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEmails_regular_email_list_with_emails_for_each_section():
    faked_email_list_file = test_file_holder[0]
    faked_email_list_file.write("[{}]\n".format(AutoDoodle.TO))
    faked_email_list_file.write("email1@gmail.com\n")
    faked_email_list_file.write("[{}]\n".format(AutoDoodle.CC))
    faked_email_list_file.write("email2@gmail.com\n")
    faked_email_list_file.write("[{}]\n".format(AutoDoodle.BCC))
    faked_email_list_file.write("email3@gmail.com\n")
    faked_email_list_file.seek(0, io.SEEK_SET)
    email_list = AutoDoodle.parseEmails(faked_email_list_file)
    verifyEmails(email_list[AutoDoodle.TO], 1, ["email1@gmail.com"])
    verifyEmails(email_list[AutoDoodle.CC], 1, ["email2@gmail.com"])
    verifyEmails(email_list[AutoDoodle.BCC], 1, ["email3@gmail.com"])


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEmails_regular_email_list_with_a_couple_emails_commented_out_and_no_sections():
    faked_email_list_file = test_file_holder[0]
    faked_email_list_file.write("email1@gmail.com\n")
    faked_email_list_file.write("email2@hotmail.com\n")
    faked_email_list_file.write("#commentedOut1@msn.com\n")
    faked_email_list_file.write("#commentedOut2@yahoo.com\n")
    faked_email_list_file.seek(0, io.SEEK_SET)
    email_list = AutoDoodle.parseEmails(faked_email_list_file)[AutoDoodle.TO]
    verifyEmails(email_list, 2, ["email1@gmail.com", "email2@hotmail.com"])


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEmails_no_emails():
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEmails(test_file_holder[0])
    assert_in("No emails were found or were all commented out", str(error.exception))


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEmails_all_emails_commented_out():
    faked_email_list_file = test_file_holder[0]
    faked_email_list_file.write("#commentedOut1@msn.com\n")
    faked_email_list_file.write("#commentedOut2@yahoo.com\n")
    faked_email_list_file.seek(0, io.SEEK_SET)
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEmails(faked_email_list_file)
    assert_in("No emails were found or were all commented out", str(error.exception))


@with_setup(setup_empty_file, teardown_faked_file)
def test_parseEmails_emails_with_invalid_format():
    faked_email_list_file = test_file_holder[0]
    faked_email_list_file.write("bad_email1\n")
    faked_email_list_file.write("bad_email2\n")
    faked_email_list_file.seek(0, io.SEEK_SET)
    with assert_raises(AutoDoodle.InvalidConfigFileError) as error:
        AutoDoodle.parseEmails(faked_email_list_file)
    assert_in("Invalid format for the following email(s):", str(error.exception))



"""
Tests for getParticipantEmails().

"""@raises(IOError)
def test_getParticipantEmails_invalid_file():
    AutoDoodle.getParticipantEmails("invalid_file")



"""
Tests for convertToSubjectDate().

"""
def test_convertToSubjectDate_valid_date():
    assert_equal("March 17, 2015", AutoDoodle.convertToSubjectDate("20150317"))


def test_convertToSubjectDate_invalid_date_format():
    with assert_raises(ValueError) as error:
        AutoDoodle.convertToSubjectDate("03-17-2015")
    assert_in("does not match format", str(error.exception))


def test_convertToSubjectDate_invalid_date():
    with assert_raises(ValueError) as error:
        AutoDoodle.convertToSubjectDate("20150229") # No leap year for 2015.
    assert_in("day is out of range for month", str(error.exception))


"""
Tests for getEmailTemplateSubstitutions().

"""
@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_getEmailTemplateSubstitutions_no_reserved_word_in_section_proxy():
    substitutions = AutoDoodle.getEmailTemplateSubstitutions("doodle_poll_url", event_details)
    assert_equal("doodle_poll_url", substitutions[AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE])


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_getEmailTemplateSubstitutions_reserved_word_in_section_proxy():
    event_details[AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE] = "url_that_overwrites_doodle_poll_url"
    substitutions = AutoDoodle.getEmailTemplateSubstitutions("doodle_poll_url", event_details)
    assert_equal("url_that_overwrites_doodle_poll_url", substitutions[AutoDoodle.DOODLE_POLL_TEMPLATE_VARIABLE])


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_getEmailTemplateSubstitutions_check_configparser_section_proxy_is_not_modified():
    event_details_copy = dict(event_details)
    AutoDoodle.getEmailTemplateSubstitutions("doodle_poll_url", event_details)
    assert_equal(event_details_copy, event_details)



"""
Tests for createEmailBody().

"""
@with_setup(setup_empty_file, teardown_faked_file)
def test_createEmailBody_all_variables_of_template_substituted():
    faked_email_template_file = test_file_holder[0]
    faked_email_template_file.write("All $variables are $substituted.\n")
    faked_email_template_file.seek(0, io.SEEK_SET)
    email_template_substitutions = {'variables':'value1', 'substituted':'value2'}
    email_body = AutoDoodle.createEmailBody(faked_email_template_file, email_template_substitutions)
    assert_equal("All value1 are value2.\n", email_body)


@with_setup(setup_empty_file, teardown_faked_file)
def test_createEmailBody_missing_substitution_for_template():
    faked_email_template_file = test_file_holder[0]
    faked_email_template_file.write("Missing $substitution.\n")
    faked_email_template_file.seek(0, io.SEEK_SET)
    email_template_substitutions = {}
    email_body = AutoDoodle.createEmailBody(faked_email_template_file, email_template_substitutions)
    assert_equal("Missing $substitution.\n", email_body)


@with_setup(setup_empty_file, teardown_faked_file)
def test_createEmailBody_extra_substitution_for_template_that_is_not_used():
    faked_email_template_file = test_file_holder[0]
    faked_email_template_file.write("Extra $substitution.\n")
    faked_email_template_file.seek(0, io.SEEK_SET)
    email_template_substitutions = {'extra_substituion':'value1', 'substitution':'value2'}
    email_body = AutoDoodle.createEmailBody(faked_email_template_file, email_template_substitutions)
    assert_equal("Extra value2.\n", email_body)



"""
Tests for getEmailBody().

"""
@raises(IOError)
def test_getEmailBody_invalid_file():
    email_template_substitutions = {}
    AutoDoodle.getEmailBody("invalid_file", email_template_substitutions)



"""
Tests for createEmail().

"""
@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createEmail_with_required_fields():
    event_details[AutoDoodle.DATE] = "20150317"
    email_to_send = AutoDoodle.createEmail("email_body", event_details, valid_email_list_all)
    assert_equal("to_participant_emails", email_to_send[AutoDoodle.TO])
    assert_equal("cc_participant_emails", email_to_send[AutoDoodle.CC])
    assert_equal("bcc_participant_emails", email_to_send[AutoDoodle.BCC])
    assert_equal("arbitrary_value <fake@email>", email_to_send['From'])
    assert_equal("arbitrary_value: March 17, 2015 5pm-7pm", email_to_send['Subject'])
    assert_equal("email_body", email_to_send.get_payload())


@with_setup(setup_event, teardown_event)
@raises(KeyError)
def test_createEmail_missing_required_field():
    AutoDoodle.createEmail("email_body", event_details, valid_email_list)


@with_setup(setup_event_with_all_required_fields, teardown_event)
@raises(ValueError)
def test_createEmail_blank_date():
    event_details[AutoDoodle.DATE] = ""
    AutoDoodle.createEmail("email_body", event_details, valid_email_list)


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createEmail_missing_participant_emails():
    event_details[AutoDoodle.DATE] = "20150317"
    with assert_raises(AutoDoodle.MissingEmailHeaderInformationError) as error:
        AutoDoodle.createEmail("email_body", event_details, {})
    assert_in("-Need emails for at least one of the fields \"To\", \"Cc\", or \"Bcc\" for the email", str(error.exception))


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createEmail_missing_participant_emails_with_other_fields():
    event_details[AutoDoodle.DATE] = "20150317"
    with assert_raises(AutoDoodle.MissingEmailHeaderInformationError) as error:
        AutoDoodle.createEmail("email_body", event_details, {"fake": []})
    assert_in("-Need emails for at least one of the fields \"To\", \"Cc\", or \"Bcc\" for the email", str(error.exception))


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createEmail_missing_sender_information():
    event_details[AutoDoodle.ADMIN_NAME] = ""
    event_details[AutoDoodle.ADMIN_EMAIL] = ""
    event_details[AutoDoodle.DATE] = "20150317"
    with assert_raises(AutoDoodle.MissingEmailHeaderInformationError) as error:
        AutoDoodle.createEmail("email_body", event_details, valid_email_list)
    assert_in("-\"From\" field for the email", str(error.exception))


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createEmail_missing_subject_information():
    event_details[AutoDoodle.TITLE] = ""
    event_details[AutoDoodle.TITLE] = ""
    event_details[AutoDoodle.DATE] = "20150317"
    with assert_raises(AutoDoodle.MissingEmailHeaderInformationError) as error:
        AutoDoodle.createEmail("email_body", event_details, valid_email_list)
    assert_in("-\"Subject\" field for the email", str(error.exception))


@with_setup(setup_event_with_all_required_fields, teardown_event)
def test_createEmail_blank_email_body():
    event_details[AutoDoodle.DATE] = "20150317"
    email_to_send = AutoDoodle.createEmail("", event_details, valid_email_list)
    assert_equal("", email_to_send.get_payload())



"""
Tests for sendEmail().

"""
@raises(AutoDoodle.InvalidLoginCredentialsError)
def test_sendEmail_missing_password():
    AutoDoodle.sendEmail("smtp_host", "message", "user_name", password=None)


@raises(AutoDoodle.InvalidLoginCredentialsError)
def test_sendEmail_missing_username():
    AutoDoodle.sendEmail("smtp_host", "message", user_name=None, password="password")


"""
The following functions intentionally have no unit tests:
1) getSeleniumBrowser()
2) submitDoodlePoll()
3) createDoodlePoll()
4) getEmailServer()

Reasons for not creating unit tests for these functions:
-The logic in these functions is fairly simple.
-Making unit tests for functions 1) to 3) would effectively be testing Seleniun components.
-They involve using external services, so integration tests are better suited to test these functions.

Integration tests are carried out manually by trying various combinations of the options provided in AutoDoodle.

"""