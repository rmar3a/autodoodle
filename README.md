# AutoDoodle #

AutoDoodle is a Python script for generating weekly Doodle polls (http://doodle.com) and sending out the participation link to the
participants of the event via a plain text email.  AutoDoodle works for Linux, Mac, and Windows.

## Contents ##

* Getting started
    * Downloading Program
    * Dependencies
* Program Details
    * Required Input Files
    * Program Parameters
    * Using AutoDoodle
* Troubleshooting
* Contact
* Developer Notes

### Getting started ###

#### Downloading Program ####

To get a copy of the program simply download and extract the zip file from the project "Overview" page on
Bitbucket (https://bitbucket.org/rmar3a/autodoodle/overview) or clone the repository using Git.

    git clone https://bitbucket.org/rmar3a/autodoodle.git

#### Dependencies ####

AutoDoodle has been written to work with Python 3.2, so make sure you have this version of Python or greater.

AutoDoodle depends on Selenium (http://www.seleniumhq.org) to automate the process of generating the Doodle polls and requires the Firefox
web browser (https://www.mozilla.org/en-US/firefox/new) or the PhantomJS headless web browser (http://phantomjs.org) to be installed.  AutoDoodle
does come with a modified version of Selenium; however, due to the rapid pace at which browsers are updated it has become obsolete
so it is recommended that the latest version of Selenium be downloaded and installed.

### Program Details ###

#### Required Input Files ####

AutoDoodle three types of input files, an event configuration file, an email list file, and an email template file.  By default the
program will use the files event_config.txt, email_list.txt, and email_template.txt which should be located in the same directory as the
AutoDoodle.py script.  All three input files can be specified using the appropriate parameters (see "Program Parameters" section below).

The script attempts to validate the contents of each of these files, which must follow the formats shown below.

##### Event Configuration File Format #####

The event configuration file that comes with AutoDoodle when downloaded for the first time should look like the following:

    [EventDetails]
    locale=en
    title=PlaceHolder
    location=PlaceHolder
    adminName=PlaceHolder
    adminEmail=PlaceHolder@PlaceHolder.com
    day=Wednesday
    time=6PM-8PM

This file contains all the details to specify for the event to create.  The section header "EventDetails" as well as all the options seen
above are required in the event configuration file.  In general the value for each option is flexible; however, "adminEmail" must have a
valid email format and "day" must be one of Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday (case-sensitive).

Note: Additional options may be put under the "EventDetails" section and used by the email template file (see "Email Template File Format"
section below).

##### Email List File Format #####

The email list file that comes with AutoDoodle when downloaded for the first time should look like the following:

    [To]
    email1
    email2
    [Cc]
    email3
    email4
    [Bcc]
    email5
    email6

This file contains all the emails to send the Doodle poll link to, i.e. emails of all the participants for the event.  Each email must be
on its own line and any email preceded by a "#" will be ignored, i.e. emails preceded by a "#" will not receive an email when the Doodle
poll link is sent out to the participants for the event.  All the emails below a certain section header are placed in the respective list
in the email that is sent out.  All section headers are optional, i.e. you can keep any combination of the headers in this file.

An alternative format for this file is the following:

    email1
    email2

If you decide to use this format, all the emails will be placed on the "To" line for the email containing the Doodle poll link that is sent
out to the participants for the event.

##### Email Template File Format #####

The email template file that comes with AutoDoodle when downloaded for the first time should look like the following:

    Hi everyone,

    Here's the Doodle for this week:
    $doodlePollUrl

    Cheers,
    $adminName

This file contains the body of the email that gets sent out to the participants of the event.  Any option that appears in the event
configuration file can be substituted into the email template by adding a "$" to the beginning of the option in the email template file.
For example, the "$adminName" in the email template above will have the value of "adminName" found in the event configuration file placed
into the email.

**IMPORTANT**: "$doodlePollUrl" is a reserved word in the email template file.  This is where the Doodle poll link will be placed in the
email that is sent, so altering the name of this variable or removing it will result in the Doodle poll link missing from the email.
However, when the --preview_email option is used "[Doodle Link Will Appear Here]" will appear here instead.

#### Program Parameters ####

The parameters available for AutoDoodle can be found by using the -h/--help option to display a usage message:

    python AutoDoodle.py -h

**WARNING**: There are no required parameters when running the script (it will use the default parameters) so be sure to configure the
three input files for AutoDoodle (see "Required Input Files" section above) and read the usage message before running the program.

The following table lists the available parameters for AutoDoodle:

Option | Description
---------|---------------
--validate_only | Only validate the event configuration file.
--doodle_only | Only create a Doodle poll.
--email_only DOODLE_POLL_LINK | Only send an email with the given Doodle poll link.
--use_firefox | Use Firefox for the browser.  [Default]
--use_phantomjs | Use PhantomJS for the browser.
--preview_email |Preview the email that will be sent to the participants of the event. No Doodle poll will be created, and no email will be sent.
-c EVENT_CONFIG_FILE, --event_config_file EVENT_CONFIG_FILE | File containing the information for the event.  [Default: event_config.txt]
-e EMAIL_LIST_FILE, --email_list_file EMAIL_LIST_FILE | File containing the list of emails to send the Doodle link to.  [Default: email_list.txt]
-b EMAIL_TEMPLATE_FILE, --email_template_file EMAIL_TEMPLATE_FILE | File containing the email body template to send to the participants of the event.  [Default: email_template_file]
-s SMTP_HOST, --smtp_host SMTP_HOST | Server that provides SMTP service. Port can be specified after the server name separated by a colon.  [Default: localhost]
-u USERNAME, --username USERNAME | The username of the email account used to send the Doodle link; not all SMTP servers require this parameter. Required parameter if -p/--password is used.
-p PASSWORD, --password PASSWORD | The password of the email account used to send the Doodle link; not all SMTP servers require this parameter. Required parameter if -u/--username is used.
-t TIMEOUT, --timeout TIMEOUT | The amount of time (in seconds) that Selenium should use to search for a browser element before giving up.  [Default: 10]
-V, --version  | Show program's version number and exit.
-h, --help | Show this message and exit.

#### Using AutoDoodle ####

After configuring all three required input files for AutoDoodle (see "Required Input Files" section), it is recommended that you run the
--validate_only option to check that you have configured the event configuration file properly and also run the --preview_email option to
verify that the email you are about to send looks correct.

AutoDoodle should work with just about any SMTP server (provided that you know the name of the SMTP server and what port to connect to);
to use the Gmail SMTP server pass the following to AutoDoodle "-s smtp.gmail.com:587".  The Gmail server requires a username and password
so it's probably best to create a new Google account and **use a password that you don't use for other services** as your password will be
visible in plain text when passing it to the "-p/--password" option.

Once the AutoDoodle script is running, the following will occur:

1.  The contents of the event configuration file will be validated.
2.  One of the following will occur depending on the browser that you are using:
    *  Firefox: A Firefox web browser will open up after a few seconds.  **DON'T TOUCH THIS WINDOW!**  It will close by itself if the automation succeeds.
    *  PhantomJS: The automation will run in the background.  You can check the output of the script to verify that the automation succeeds.
3.  The Doodle poll will already be pre-filled and Selenium will automatically click all the necessary buttons and close the browser for you.
4.  The link will get sent out to all the emails found in the provided email list file using the SMTP server provided or "localhost" on port 25
    if no SMTP server is provided.

**IMPORTANT**: If the automation fails after the browser has launched successfully, a screenshot file ("autodoodle-error.png") will be generated
to help indicate which part of the process failed.

In general, the event will be scheduled on the same week that the script is run, given that the day of the week for the event hasn't passed
yet.  If the script is run the day after the event was supposed to occur for that week, then the Doodle poll will be created for next week.
For example, if the script is run on Tuesday and the event is every Thursday, then the Doodle poll will be for the Thursday of that week.  On the
otherhand, if the script is run on Tuesday and the event is every Monday, then the Doodle poll will be for the Monday next week.

Once you place your AutoDoodle command that successfully creates the Doodle poll and sends out the email into a cron job (Linux or Mac) or
place it into Windows Task Scheduler (Windows) you'll never have to create the weekly Doodle polls manually ever again, hurray!

### Troubleshooting ###

In general, the script handles all known issues so read any error messages that are printed out and correct the problem accordingly.
However, if you are having trouble getting Selenium to start up the browser and create the Doodle poll try to update your copy of Selenium.

### Contact ###

If there are any issues to report please check the issue tracker on Bitbucket to see if it has already been reported.  Please create a new
issue if the problem hasn't been reported yet.  For all other inquires, please send a message via Bitbucket's internal messaging system to
Richard (rmar3a).

### Developer Notes ###

The tests for AutoDoodle are written using the Python testing framework nose (https://nose.readthedocs.org/en/latest).  To run the tests,
simply run the nosetests command in the AutoDoodle directory.
